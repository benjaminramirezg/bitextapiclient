use Mojo::UserAgent;

my $url = "http://svc8.bitext.com/WS_NOps_Val/Service.aspx";
my $usr = 'My user';
my $pwd = 'My password';
my $txt = 'The API is very good';
my $id = '0001';
my $lang='ENG';

my %parameters = (User => $usr, Pass => $pwd, Lang => $lang, 
		  ID => $id, Text => $txt, Detail => 'Detailed', 
		  OutFormat => 'JSON', Normalized => 'No', Theme => 'Gen');

my $ua = Mojo::UserAgent->new();
my $tx = $ua->post($url, form => \%parameters);

$tx->res->code eq 200 ? 
    print $tx->res->text :
    print "Bad response. Status ".$tx->res->code."\n";

